+++
title="Week 1 Mini-Project"
date=2024-01-30

[taxonomies]
categories = ["Project"]
tags = ["mini-Project"]
[extra.author]
name = "Famous author"
+++

Create a static site with ZolaLinks to an external site., a Rust static site generator, that will hold all of the portfolio work in this class.  Store source code in a GitLab repo in our Duke GitLab organization.